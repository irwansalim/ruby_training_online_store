module ApplicationHelper
  def welcome_text
    str = "" # if the user has logged in, show the welcome text.
    if current_user
      str = "Welcome, #{current_user.name} | "
      str += link_to "Logout", log_out_path
    else
      str = "Welcome, Guest | "
		  str += "#{link_to "Login", log_in_path} | "
      str += link_to "Signup", sign_up_path
    end
  end 
	
	def category_option
	  str = "" # if the user has logged in, show the welcome text.
    if is_admin
      str = link_to "Edit ", edit_admin_category_path
			str += link_to "Delete", admin_category_path, :method => :delete
    else
      str = ""
    end
	end
end
