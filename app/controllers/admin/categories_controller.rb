class Admin::CategoriesController < Admin::ApplicationController
  protect_from_forgery
	before_filter :require_admin_login
	
	def new
	  @category = Category.new
  end
	
	def create
	  #raise params[:product].inspect
	  @category = Category.new(params[:category])
		if @category.save
		  flash[:notice] = 'Your Category was succesfully created'
			redirect_to root_path
		else
		  flash[:error] = 'Your Category was failed to create'
			render :action => "new"
		end
	end
	
	def edit
	  @category = Category.find_by_id(params[:id])
  end
	
	def update
	  @category = Category.find_by_id(params[:id])
		if @category.update_attributes(params[:category])
		  flash[:notice] = 'Category was successfully updated'
			redirect_to edit_admin_category_path
		else
		  flash[:error] = 'Category was failed to update'
			redirect_to edit_admin_category_path
		end
	end
	
  def destroy
    @category = Category.find(params[:id])
    @category.destroy
    redirect_to root_path#:controller => :categories, :action => :index
		flash[:notice] = 'Category was successfully deleted.'
  end
end

