class CategoriesController < ApplicationController
  #before_filter :require_login, :only => [:index]
	#before_filter :require_admin_login, :only => [:new, :create, :edit, :update, :delete]
	 
  def index
		@categories = Category.all
		@parent = Category.find_by_id(params[:id])
  end
	
	#def destroy
  #  @category = Category.find(params[:id])
  #  @category.destroy
  #  redirect_to :controller => :categories, :action => :index
	#	flash[:notice] = 'category was successfully deleted.'
  #end
end
