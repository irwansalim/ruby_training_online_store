class ProductsController < ApplicationController
  def index
	  @products = Product.all
  end
	
	def show
		@product = Product.find_by_id(params[:id])
  end

  def edit
		@product = Product.find_by_id(params[:id])
  end
	
	def update
	  @product = Product.find_by_id(params[:id])
		if @product.update_attributes(params[:product])
		  flash[:notice] = 'Your product was succesfully updated'
			render :action => "edit"
		else
		  flash[:error] = 'Your product was failed to update'
			render :action => "edit"
		end
	end

  def new
	  @product = Product.new
  end

  def create
	  @product = Product.new(params[:product])
		if @product.save
		  flash[:notice] = 'Your product was succesfully created'
			redirect_to root_path
		else
		  flash[:error] = 'Your product was failed to create'
			render :action => "new"
		end
	end
  
end
