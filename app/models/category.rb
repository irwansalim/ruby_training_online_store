class Category < ActiveRecord::Base
  # attr_accessible :title, :body
	has_many :products,  :dependent => :destroy # plural
	#belongs_to :parent
	#has_many :children, :foreign_key => :parent_id
	
	validates :name, :presence => true
	#validates :parent_id, :presence => true
end
