class User < ActiveRecord::Base
  # attr_accessible :title, :body
	has_many :products,  :dependent => :destroy # plural
	has_many :articles,  :dependent => :destroy # plural
	
	validates :email, :presence => true,
	                  :length => {:minimum => 3, :maximum => 254},
                    :uniqueness => true,
                    :format => {:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}
	#validates :password_hash, :presence => true
	#validates :password_salt, :presence => true
	validates :name, :presence => true,
	                 :length => {:minimum => 1, :maximum => 254},
                   :format => {:with => /[a-zA-Z\s]+$/}
	validates :address, :presence => true
	#validates :phone_number, :presence => true
	#validates :birthday, :presence => true
	validates :gender, :presence => true
	#validates :facebook, :presence => true
	#validates :blog, :presence => true
	
	validates :password, :presence => {:on => :create},
                     :confirmation => true
	
	def is_admin?
		self.email == "admin@admin.com"
	end
	
	#LOGIN
	attr_accessor :password, :password_confirmation
	before_save :encrypt_password
	
	def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end
  
	def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
	end
end
