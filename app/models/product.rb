class Product < ActiveRecord::Base
  # attr_accessible :title, :body
	belongs_to :user
	belongs_to :category

	
	validates :name, :presence => true
	validates :description, :presence => true
	validates :price, :presence => true
	#validates :weight, :presence => true
	validates :user_id, :presence => true
	validates :category_id, :presence => true
end
