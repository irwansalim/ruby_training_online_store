class Comment < ActiveRecord::Base
  # attr_accessible :title, :body
	belongs_to :article
	
	validates :comment, :presence => true
	validates :article_id, :presence => true
	validates :user_id, :presence => true
end
