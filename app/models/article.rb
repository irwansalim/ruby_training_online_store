class Article < ActiveRecord::Base
  # attr_accessible :title, :body
	has_many :comments,  :dependent => :destroy # plural
	belongs_to :user
	
	validates :title, :presence => true
	validates :body, :presence => true
	validates :user_id, :presence => true
end
