# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
users = User.create([
#{:email => "admin@rubystore.com", :password_hash => "123456", :password_salt => "123456", :name => "Admin's Name", :address => "Admin's Home Address", :phone_number => "Admin's Phone Number", :birthday => 1981-10-10, :gender => true, :facebook => "adminfb", :blog =>"adminblog"}
{:email => "david@jimail.com", :password_hash => "123456", :password_salt => "123456", :name => "David", :address => "David's Home Address", :phone_number => "David's Phone Number", :birthday => 1982-11-11, :gender => true, :facebook => "davidfb", :blog =>"davidblog"},
{:email => "cassanda@yemail.com", :password_hash => "123456", :password_salt => "123456", :name => "Cassanda", :address => "Cassanda's Home Address", :phone_number => "Cassanda's Phone Number", :birthday => 1983-12-12, :gender => false, :facebook => "cassandafb", :blog =>"cassandablog"}
])

categories = Category.create([
{:name => "Automotive", :parent_id => NIL},
{:name => "Food", :parent_id => NIL},
{:name => "Car", :parent_id => 1},
{:name => "Motorcycles", :parent_id => 1},
{:name => "Air Plane", :parent_id => 1},
{:name => "Eastern", :parent_id => 2},
{:name => "Western", :parent_id => 2}
])

products = Product.create([
{:name => "Fortuner", :description => "Car from Toyota", :price => 40000, :weight => 1000, :user_id => 1, :category_id => 3},
{:name => "Civic", :description => "Car from Honda", :price => 42000, :weight => 900, :user_id => 2, :category_id => 3},
{:name => "Ninja 650", :description => "Motorcycles from Kawasaki", :price => 25000, :weight => 500, :user_id => 2, :category_id => 4},
{:name => "747", :description => "Air Plane from somewhere", :price => 1000000, :weight => 6000, :user_id => 1, :category_id => 5},
{:name => "Sirloin Steak", :description => "Beef, fries, and tomato sauce", :price => 10, :weight => 1, :user_id => 3, :category_id => 7},
{:name => "Hot Ramen", :description => "Spicy ramen with eggs", :price => 8, :weight => 2, :user_id => 3, :category_id => 6},
{:name => "Cheese Burger", :description => "Burger with beef and cheese", :price => 11, :weight => 1, :user_id => 2, :category_id => 7},
{:name => "Beef Kebab", :description => "Kebab with beef and ring onions", :price => 12, :weight => 1, :user_id => 3, :category_id => 6}
])

articles = Article.create([
{:title => "Article About Car", :body => "Article content about car", :user_id => 1},
{:title => "Article About MotorCycles", :body => "Article content about motorcycles", :user_id => 1},
{:title => "Article About Air Plane", :body => "Article content about air plane", :user_id => 2},
{:title => "Article About Automotive", :body => "Article content about automotive", :user_id => 2},
{:title => "Article About Food", :body => "Article content about food", :user_id => 3},
{:title => "Article About Ramen", :body => "Article content about ramen", :user_id => 3},
{:title => "Article About Burger", :body => "Article content about burger", :user_id => 3}
])

comments = Comment.create([
{:comment => "Nice articles, thanks for share!", :article_id => 1, :user_id => 1},
{:comment => "Agree.", :article_id => 2, :user_id => 1},
{:comment => "Good job mate!", :article_id => 3, :user_id => 2},
{:comment => "No doubt.", :article_id => 4, :user_id => 2},
{:comment => "Great! Let's try this right now!", :article_id => 5, :user_id => 3},
{:comment => "Perfect!!", :article_id => 6, :user_id => 3},
{:comment => "Wow! Good idea.", :article_id => 7, :user_id => 3}
])